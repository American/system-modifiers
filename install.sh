#!/bin/bash
## This Bash script installs the system-modifiers scripts to allow usage anywhere in the terminal.
## satanism@riseup.net
## twitter.com/proanarchism
## github.com/9-5
## keybase.io/38
## This project can be found at https://github.com/9-5/system-modifiers
## It can also be found at https://gitlab.com/American/system-modifiers

sudo_chk ( ) {
    script="system-modifiers"
    if [ "$(id -nu)" != "root" ]; then
        sudo -k
        pass=$(whiptail --backtitle "$script Installer" --title "Authentication required." --passwordbox "Installing $script requires sudo access. Please authenticate to begin the installation.\n\n[sudo] Password for user $USER:" 12 50 3>&2 2>&1 1>&3-)
        exec sudo -S -p '' "$0" "$@" <<< "$pass"
        exit 1
    fi
}

ebin_chk ( ) {
    if [ -d /ebin ]
        then
            sudo rm -rf /ebin
        else
            sudo mkdir /ebin
    fi
}
               

tmp_input_d=$(mktemp -d /tmp/tmp.d.XXXX)

echo "alias tp='sudo /ebin/system-modifiers/scripts/tp'" >> ~/.bashrc && echo "alias kb='sudo /ebin/system-modifiers/scripts/kb'" >> ~/.bashrc && echo "alias wc='sudo /ebin/system-modifiers/scripts/wc'" >> ~/.bashrc && echo "alias mic='sudo /ebin/system-modifiers/scripts/mic'" >> ~/.bashrc && echo "alias ipv6='sudo /ebin/system-modifiers/scripts/ipv6'" >> ~/.bashrc && echo "alias hosts='sudo /ebin/system-modifiers/scripts/hosts'" >> ~/.bashrc # Adds the commands to the local user.
sudo_chk
ebin_chk
echo "Checking if system-modifiers has been downloaded yet..."
if [ ! -f ./system-modifiers ]
    then
        echo "system-modifiers has not been properly setup."
        echo "Setting up system-modifiers..."
        cd $tmp_input_d
        git clone https://gitlab.com/American/system-modifiers.git
        echo "Finished setup."
        echo "Installing system-modifiers..."
        sudo chmod +x $tmp_input_d/system-modifiers/scripts/*
        cat $tmp_input_d/system-modifiers/aliases >> ~/.bashrc # This adds the aliases to the root user.
        sudo mv $tmp_input_d/system-modifiers /ebin/ &&
        echo "system-modifiers has been installed."
    else
        echo "Installing system-modifiers..."
        sudo mv ./system-modifiers /ebin
        sudo chmod +x /ebin/system-modifiers/scripts/*
        cat /ebin/system-modifiers/aliases >> ~/.bashrc # This adds the aliases to the root user.
        echo "system-modifiers has been installed."
fi
